from django.shortcuts import render

portfolio = 'portfolio'
# Create your views here.
def index(request):
    response = {'portfolio': portfolio}
    return render(request, 'portfolio.html', response)