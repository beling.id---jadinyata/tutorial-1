from django.shortcuts import render

my_experience = 'Experience'
# Create your views here.
def index(request):
    response = {'my_experience': my_experience}
    return render(request, 'my_experience.html', response)