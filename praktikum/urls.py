"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1
from lab_1.views import message_post as message_post
from lab_1.views import message_table as message_table
from my_experience.views import index as my_experience_index
from lab_4.views import index as lab_4
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^accounts/', include('django.contrib.auth.urls')),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^my-experience/', include('my_experience.urls')),
    re_path(r'^portfolio/', include('portfolio.urls')),
    re_path(r'^message_post/', message_post, name='message_post'),
    re_path(r'^message_table/', message_table, name='message_table'),
    re_path(r'^chat/', include('lab_4.urls')),
    re_path(r'^$', index_lab1, name='index')
]
